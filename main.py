# -*- coding: utf-8 -*-
#!/usr/bin/env python3
try:
    import config
except:
    raise FileNotFoundError("Config file does not exist. Please read the README.")
import dataset
import datetime
import errno
import logging
import praw
import os
import sys
from logging.handlers import RotatingFileHandler


logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
try:
    os.makedirs("log")
except OSError as e:
    if e.errno != errno.EEXIST:
        raise
handler = RotatingFileHandler('log/log.log',
                              maxBytes=1000000,
                              backupCount=10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [MAIN] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

db = dataset.connect('sqlite:///db.db')
table = db['submissions']


def main():
    try:
        logger.info("Reddit Submission Notifier Bot started.")
        logger.info("Getting Reddit session.")
        try:
            reddit_session = praw.Reddit(client_id=config.CLIENT_ID,
                                         client_secret=config.CLIENT_SECRET,
                                         username=config.USERNAME,
                                         password=config.PASSWORD,
                                         user_agent=config.USER_AGENT
                                         )
        except Exception as e:
            logger.exception(e)
        logger.info("Got the Reddit session. Trying to obtain the subreddit.")
        sub = reddit_session.subreddit(config.SUB)
        logger.info("Got the subreddit. Gathering new submissions.")
        submissions = sub.new(limit=1000)

        message = "Hey {}! \n\n" \
                  "I've got some new submissions from /r/{} for you: \n\n" \
                  "{}" \
                  "Take care, {}"

        submissions_text = ""
        counter = 0
        for submission in submissions:
            if not table.find_one(sub_id=submission.id):
                counter += 1
                submissions_text += "* {}: [{}]({})\n\n".format(
                    datetime.datetime.fromtimestamp(submission.created).strftime('%d.%m.%Y %H:%M:%S'),
                    submission.title,
                    submission.shortlink)
                table.insert(
                    dict(
                        sub_id=str(submission.id)
                    )
                )

        if counter != 0:
            logger.info("Got {} submissions. Sending notifications.".format(counter))
            for user in config.USERS:
                try:
                    logger.info("Sending notification to /u/{}".format(user))
                    reddit_message = message.format(user, config.SUB, submissions_text, config.USERNAME)
                    redditor = reddit_session.redditor(name=user)
                    redditor.message(
                        subject="New submissions in /r/{}".format(config.SUB),
                        message=reddit_message
                    )
                except Exception as e:
                    logger.exception(e)
        else:
            logger.info("Got {} submissions. Nothing to do!".format(counter))
        logger.info("Done.")
    except KeyboardInterrupt as e:
        logger.exception(e)
        sys.exit(0)
    except Exception as e:
        logger.exception(e)

if __name__ == '__main__':
    main()

