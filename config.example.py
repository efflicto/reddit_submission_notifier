# -*- coding: utf-8 -*-
#!/usr/bin/env python3
# Enter your subreddit here
SUB = "yoursubreddit"
# You can change your user agent here but that's not necessary
USER_AGENT = "Reddit Submission Notifier bot by /u/efflicto written for /r/{}".format(SUB)

# Your Reddit client ID
CLIENT_ID = ""
# Your Reddit client secret
CLIENT_SECRET = ""
# Your username or your bots username
USERNAME = ""
# Your password or your bots password
PASSWORD = ""
# Reddit usernames to notify about a new submission
USERS = [
    "user1",
    "user2"
]