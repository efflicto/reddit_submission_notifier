## Reddit Submission Notifier
Notifies a list of users for new submissions on a specified subreddit.

##### Installation:
* You need Python 3.6
* pip install -r requirements.txt
* Copy the config.example.py to config.py and make your changes as described in the file
* Run it and enjoy